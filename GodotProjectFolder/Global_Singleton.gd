extends Node

var loader
var wait_frames
var time_max = 100 # msec
var current_scene = null
var level_amount = 5
var can_pause = true
var is_game_paused = false
var pause_popup = preload("res://Scenes/Pause_Popup/PausePopup.tscn")
var pause_popup_instance = null
var loading_scene = preload("res://Scenes/Loading_Screen/Loading.tscn")
var loading_scene_instance = null
var global_audio_stream_scene = preload("res://Scenes/Global_Audio_Stream/GlobalAudioStream.tscn")
var global_audio_stream_scene_instance = null
var mainmenu_path = "res://Scenes/Main_Menu/Mainmenu.tscn"
var previous_mouse_state = Input.MOUSE_MODE_VISIBLE
var scene_code_str = ""


const SAVE_PATH = "user://config.cfg"
var _config_file = ConfigFile.new()
var _settings = {
	"Player" : {
		"Name" : "default",
		"UnlockedLevels" : 
		[true,
		false,
		false,
		false,
		false]
	},
	"Game": {
		"MasterVolume" : 0,
		"Fullscreen" : false
	}
}

func _ready():
	self.set_pause_mode(2)
	var root = get_tree().get_root()
	global_audio_stream_scene_instance = global_audio_stream_scene.instance()
	add_child(global_audio_stream_scene_instance)
	current_scene = root.get_child(root.get_child_count() - 1)
	
	load_settings()
	save_settings()
	apply_settings()

func _process(time):
	if loader == null:
		set_process(false)
		return
	# wait for frames to let the "loading" animation show up
	if wait_frames > 0:
		wait_frames -= 1
		return
	var t = OS.get_ticks_msec()
	while OS.get_ticks_msec() < t + time_max: 
		# use "time_max" to control for how long we block this thread
		# poll your loader
		var err = loader.poll()
		if err == ERR_FILE_EOF: # Finished loading.
			var resource = loader.get_resource()
			loader = null
			set_new_scene(resource)
			break
		elif err == OK:
			update_progress()
		else: # error during loading
			loader = null
			break

"""------------------------HANDLING THE SCENE SWITCH/LOAD------------------------"""

func goto_scene(path):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene. 
	# used to change to SMALL scene and avoid loading animation
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
	#this is an internal function and should not be called directly
	#always call goto_scene(path) if a scene change is required
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)

func goto_scene_loading(path):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene. 
	# used to switch to HEAVY scene and start a loading animation
	loader = ResourceLoader.load_interactive(path)
	if loader == null:
		return
	set_process(true)
	current_scene.queue_free()
	#prepare the loading screen here
	loading_scene_instance = loading_scene.instance()
	add_child(loading_scene_instance)
	wait_frames = 1

func update_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	# Update your progress here
	loading_scene_instance.set_progress(progress*100)

func set_new_scene(scene_resource):
	loading_scene_instance.queue_free()
	current_scene = scene_resource.instance()
	get_node("/root").add_child(current_scene)

func reload_scene():
		var to_reload = current_scene.filename
		goto_scene(to_reload)

func set_scene_code(code:String):
	scene_code_str = code
	global_audio_stream_scene_instance.theme_change(code)

func get_scene_code():
	return scene_code_str

"""------------------------HANDLING THE GAME SETTINGS------------------------"""

func save_settings():
	for section in _settings.keys():
		for key in _settings[section].keys():
			_config_file.set_value(section, key, _settings[section][key])
	_config_file.save(SAVE_PATH)

func load_settings():
	var error = _config_file.load(SAVE_PATH)
	if error != OK:
		return -1
	for section in _settings.keys():
		if not _config_file.has_section(section):
			continue
		for key in _settings[section].keys():
			if not _config_file.has_section_key(section,key):
				continue
			var val = _config_file.get_value(section,key)
			if key == "UnlockedLevels" and val.size() != level_amount:
				continue 
			_settings[section][key] = val
	return 0

func get_setting(category, key):
	return _settings[category][key]

func set_setting(category, key, value):
	_settings[category][key] = value

func apply_settings():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), _settings["Game"]["MasterVolume"])


"""------------------------HANDLING PAUSE/EXIT------------------------"""

func _input(event):
	if Input.is_action_just_pressed("ui_cancel") and (can_pause or is_game_paused):
		game_pause(!is_game_paused)

func popup_button_pressed(button):
	if button == "continue":
		game_pause(false)
	elif button == "tomenu":
		game_pause(false)
		goto_scene(mainmenu_path)
	elif button == "restart":
		game_pause(false)
		reload_scene()
	if button == "apply":
		apply_settings()
	elif button == "fullscreen":
		OS.window_fullscreen = !OS.window_fullscreen
		set_setting("Game","Fullscreen",OS.window_fullscreen)

func game_pause(pause:bool):
	is_game_paused = pause
	if pause:
		previous_mouse_state = Input.get_mouse_mode()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		pause_popup_instance = pause_popup.instance()
		pause_popup_instance.connect("button_pressed",self,"popup_button_pressed")
		add_child(pause_popup_instance)
	else:
		Input.set_mouse_mode(previous_mouse_state)
		pause_popup_instance.queue_free()
	get_tree().paused = pause

func set_pausable(pause:bool):
	can_pause = pause

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		save_settings()
		get_tree().quit() # default behavior