extends KinematicBody

#Variable Declarations
#This scripts is used for all player movement and interactions

var SPEED = 8 #Players Speed
var ROTATION_SPEED_TRESHOLD = 0.6
var MAX_GRAVITY_SPEED = 25
var UP = Vector3(0, 1, 0) #So GODOT can detect whats a wall and floor
export var JUMP_SPEED = 13
export var CLIMB_SPEED = 4
var camera
var camera_transform

var teleport = false

#new code
var gravity = -9.8
var gravity_strenght = 3
var velocity = Vector3(0,0,0)
const ACCELERATION = 5
const DE_ACCELERATION = 8
var is_moving = false
var was_moving = false

var gravity_speed = 0 #Gravity

#Which dimension the players in
var dimension = 0
var on_floor_trigger = true


#Jumping Variables
var jump = false
var jump_timer_trigger = false
var ladder = false
var hurt_sound = false
signal jump
signal dead
signal moving(moving)
#Movement Vector
var movement = Vector3(0, 0, 0)


#Called Once
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	camera = get_node("Target/Camera")
	$"/root/Global_Singleton".set_scene_code("dimension_past")


#Called every physics frame Calls all Movement Functions
func _physics_process(delta):
	#Resetting Movement Vector
	#Calling Functions
	camera_transform = camera.get_global_transform()
	move(delta)
	climb()
	apply_vel()
	update_rotation()
	
	if (!is_on_floor()):
		on_floor_trigger = true
	


func move(delta):
	
	var dir = Vector3()
	is_moving = false
	if Input.is_action_pressed("ui_up"):
		dir += -camera_transform.basis.z
		is_moving = true
	if Input.is_action_pressed("ui_down"):
		dir += camera_transform.basis.z
		is_moving = true
	if Input.is_action_pressed("ui_right"):
		dir += camera_transform.basis.x
		is_moving = true
	if Input.is_action_pressed("ui_left"):
		dir += -camera_transform.basis.x
		is_moving = true
	if Input.is_action_pressed("ui_jump") and is_on_floor():
		jump = true
		emit_signal("jump")
	
	if was_moving != is_moving:
		emit_signal("moving",is_moving)
	was_moving = is_moving
	dir.y = 0
	dir = dir.normalized()
	
	velocity.y += gravity*gravity_strenght*delta
	if jump:
		velocity.y = JUMP_SPEED
		jump = false
	if ladder:
		velocity.y = 0
	var hv = velocity
	hv.y = 0
	
	var new_pos = dir*SPEED
	var accel = DE_ACCELERATION
	if(dir.dot(hv) > 0):
		accel = ACCELERATION
	
	hv = hv.linear_interpolate(new_pos,accel*delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
	
	
	#Changing Dimensions 
	#Player teleports 50 units up to Dimension 2
	#Player teleports -50 units down to Dimension 1
	if Input.is_action_just_pressed("ui_teleport"):
		if dimension == 0:
			set_translation(Vector3(get_translation().x, get_translation().y + 50, get_translation().z))
			dimension = 1
			$"/root/Global_Singleton".set_scene_code("dimension_present")
		else:
			set_translation(Vector3(get_translation().x, get_translation().y - 50, get_translation().z))
			dimension = 0
			$"/root/Global_Singleton".set_scene_code("dimension_past")

#Climbing Ladder
func climb():
	if ladder == true and Input.is_action_pressed("ui_jump"):
		velocity.y = CLIMB_SPEED
	elif (ladder == true and Input.is_action_pressed("ui_descend")):
		velocity.y = -CLIMB_SPEED


func apply_vel():
	velocity = move_and_slide(velocity, UP, true, 4, PI/4, false)


func update_rotation():
	var hz_velocity = Vector2(velocity.x,velocity.z)
	if is_moving and hz_velocity.length() > SPEED*ROTATION_SPEED_TRESHOLD:
		var angle = atan2(hz_velocity.x,hz_velocity.y)
		var character_rot = get_rotation()
		character_rot.y = angle
		set_rotation(character_rot)

#Functions used outside of Player
#NB this way of handling respawn can be dangerous, the hazard can directly call 
# $"/root/Global_Singleton".reload_scene() to make the player respawn
#it's good untill we need to move object and restart their position
#on player death
func dead():
	emit_signal("dead")
	if dimension == 0:
		get_node("../Spawn_point_1").respawn()
	if dimension == 1:
		get_node("../Spawn_point_2").respawn()

func is_moving():
	return is_moving
