extends Spatial

onready var player = get_node("..")


#Jumping Sounds
var jump_trigger = false
var jumping_insert = "Jump_"
var jumping = ""
var jump_vfx_amount = 3

#Hurt Sounds
var hurt_insert = "Hurt_"
var hurt = ""
var hurt_vfx_amount = 3

#Footstep Sounds
var step_trigger = false
var step_insert = "Footstep_Metal_"
var step = ""
var step_vfx_amount = 12
var step_timer = null

#Teleporting Sounds
var teleport_insert = "Teleport_"
var teleport = ""
var teleport_vfx_amount = 2

func _ready():
	step_timer = get_node("../Footstep_Timer")


func _on_Player_moving(moving):
	if moving:
		step = step_insert + str(randi()%step_vfx_amount+1)
		get_node(step).play()
		step_timer.start()
	else:
		step_timer.stop()


func _on_Footstep_Timer_timeout():
	if (player.is_on_floor() and player.ladder == false):
		step = step_insert + str(randi()%step_vfx_amount+1)
		get_node(step).play()


func _input(event):
	if Input.is_action_just_pressed("ui_teleport"):
		teleport = teleport_insert + str(randi()%teleport_vfx_amount+1)
		get_node(teleport).play()


func _on_Player_jump():
	jumping = jumping_insert + str(randi()%jump_vfx_amount+1)
	get_node(jumping).play()


func _on_Player_dead():
	hurt = hurt_insert + str(randi()%hurt_vfx_amount+1)
	get_node(hurt).play()