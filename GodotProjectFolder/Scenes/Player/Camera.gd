extends Camera

var mouse_pos = Vector2()
var can_pass = false
var look_position = Vector3()

#Can only equal 0 or 1
var camera_spot = 0
var camera_transition = 0

func _physics_process(delta):
	mouse_ray()
	#Changing Camera Translation
	if (Input.is_action_just_pressed("ui_teleport") and camera_spot == 0):
		camera_spot = 1
		set_translation(Vector3(get_translation().x, get_translation().y + 50,get_translation().z))
	else:
		if (Input.is_action_just_pressed("ui_teleport")):
			camera_spot = 0
			set_translation(Vector3(get_translation().x, get_translation().y - 50,get_translation().z))

#Shooting a Raycast out of the camera to get mouse position
func mouse_ray():
	var ray_length = 1000
	mouse_pos = get_viewport().get_mouse_position()
	var camera = self
	var from = camera.project_ray_origin(mouse_pos)
	var to = from + camera.project_ray_normal(mouse_pos) * ray_length
	
	var space_state = get_world().get_direct_space_state()
	# use global coordinates, not local to node
	var result = space_state.intersect_ray( from, to )
	if (len(result) > 0):
		can_pass = true
		look_position = result['position']
	else:
		can_pass = false
