extends Camera

export var distance = 20.0
export var height = 15.0
export var TURNING_SPEED = PI/2
var mouse_movement = Vector2()

func _ready():
	set_as_toplevel(true)
	set_physics_process(true)

func _physics_process(delta):
	var target = get_parent().get_global_transform().origin
	var pos = get_global_transform().origin
	
	var offset = pos - target
	offset = offset.normalized()*distance
	offset.y = height
	
	offset = offset.rotated(Vector3.UP,-TURNING_SPEED*mouse_movement.x/1024)
	mouse_movement = Vector2()
	pos = target+offset
	look_at_from_position(pos,target,Vector3.UP)

func _input(event):
	if event is InputEventMouseMotion:
		mouse_movement = event.relative
