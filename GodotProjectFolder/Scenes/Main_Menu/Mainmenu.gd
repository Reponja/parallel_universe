extends Node

export var path_to_button_container : NodePath

func _ready():
	var button_array = get_node(path_to_button_container).get_children()
	var unlocked_array = $"/root/Global_Singleton".get_setting("Player","UnlockedLevels")
	for i in range(button_array.size()):
		button_array[i].disabled = !unlocked_array[i]
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$"/root/Global_Singleton".set_scene_code("menu")

func _on_Level1_pressed():
	$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_1/Level_1.tscn")

func _on_Level2_pressed():
	$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_2/Level_2.tscn")

func _on_Level3_pressed():
	$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_3/Level_3.tscn")

func _on_Level4_pressed():
	$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_4/Level_4.tscn")

func _on_Level5_pressed():
	$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_5/Level_5.tscn")