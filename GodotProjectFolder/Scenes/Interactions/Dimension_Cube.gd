extends KinematicBody

var MAX_GRAVITY_SPEED = 25
var gravity_speed = 0

var forward = false
var backward = false
var left = false 
var right = false
var is_picked_up = false


var movement = Vector3()
var dimension = 1

var SPEED = 6
var dir = Vector3()
var velocity = Vector3()
var gravity = -9.8
var gravity_strenght = 6
var DE_ACCELERATION = 7
var ACCELERATION = 3
var relative_direction = Vector2()

#Moving Based on which side players touching
func _physics_process(delta):

	if (Input.is_action_just_pressed("ui_teleport") and dimension == 1 and is_picked_up):
		dimension = 2
		set_translation(Vector3(get_translation().x,get_translation().y + 50, get_translation().z))
	elif (Input.is_action_just_pressed("ui_teleport") and dimension == 2 and is_picked_up):
		dimension = 1
		set_translation(Vector3(get_translation().x, get_translation().y - 50, get_translation().z))
	
	var dir = Vector3()
	dir.x = relative_direction.x
	dir.z = relative_direction.y
	dir.y = 0
	dir = dir.normalized()
	
	velocity.y += gravity*gravity_strenght*delta
	var hv = velocity
	hv.y = 0
	
	var new_pos = dir*SPEED
	var accel = DE_ACCELERATION
	if(dir.dot(hv) > 0):
		accel = ACCELERATION
	
	hv = hv.linear_interpolate(new_pos,accel*delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
	velocity = move_and_slide(velocity, Vector3.UP, true, 4, PI/4, false)


func _on_PickUpArea_body_entered(body):
	if (body.is_in_group("Player")):
		$Glowing.show()
		is_picked_up = true

func _on_PickUpArea_body_exited(body):
	if (body.is_in_group("Player")):
		$Glowing.hide()
		is_picked_up = false



func _on_PushingArea_body_entered(body):
	if (body.is_in_group("Player")):
		var player_pos = body.get_global_transform().origin
		var pos = get_global_transform().origin
		var direction_3d = pos - player_pos
		
		if abs(direction_3d.x) > abs(direction_3d.z):
			relative_direction = Vector2.RIGHT*direction_3d.x
		else:
			relative_direction = Vector2.DOWN*direction_3d.z


func _on_PushingArea_body_exited(body):
	if (body.is_in_group("Player")):
		relative_direction = Vector3(0,0,0)
