extends Spatial

var climb = false
var player_body = 0

func _physics_process(delta):
	if (climb == true and Input.is_action_pressed("ui_up")):
		player_body.movement.y -= 1
		player_body.ladder = true

func _on_Area_body_entered(body):
	if (body.get_name() == "Player"):
		climb = true
		player_body = body

func _on_Area_body_exited(body):
	player_body.ladder = false
	climb = false
	player_body = 0


