extends Spatial

func _on_Area_body_entered(body):
	if (get_node("..").get_name() == "Level_1"):
		var array_unlocked = $"/root/Global_Singleton".get_setting("Player","UnlockedLevels")
		array_unlocked[1] = true
		$"/root/Global_Singleton".set_setting("Player","UnlockedLevels",array_unlocked)
		$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_2/Level_2.tscn")
	if (get_node("..").get_name() == "Level_2"):
		var array_unlocked = $"/root/Global_Singleton".get_setting("Player", "UnlockedLevels")
		array_unlocked[2] = true
		$"/root/Global_Singleton".set_setting("Player","UnlockedLevels",array_unlocked)
		$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_3/Level_3.tscn")
	if (get_node("..").get_name() == "Level_3"):
		var array_unlocked = $"/root/Global_Singleton".get_setting("Player", "UnlockedLevels")
		array_unlocked[3] = true
		$"/root/Global_Singleton".set_setting("Player","UnlockedLevels",array_unlocked)
		$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_4/Level_4.tscn")
	if (get_node("..").get_name() == "Level_4"):
		var array_unlocked = $"/root/Global_Singleton".get_setting("Player", "UnlockedLevels")
		array_unlocked[4] = true
		$"/root/Global_Singleton".set_setting("Player","UnlockedLevels",array_unlocked)
		$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Levels/Level_5/Level_5.tscn")
	if (get_node("..").get_name() == "Level_5"):
		$"/root/Global_Singleton".goto_scene_loading("res://Scenes/Main_Menu/Mainmenu.tscn")