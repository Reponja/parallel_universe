extends Spatial

#0 not pressed, 1 pressed
var position = 0
var trigger = false

#If the button is pressed down
var pressed = false

signal pressed
signal released

func _on_Collision_Detection_body_entered(body):
	if (body.is_in_group("Dimension Cube")):
		$Press.play()
		$"Cylinder".set_translation(Vector3(0,-0.13,0))
		#get_tree().call_group("Gate", "open")
		emit_signal("pressed")

func _on_Collision_Detection_body_exited(body):
	if (body.is_in_group("Dimension Cube")):
		$Release.play()
		$"Cylinder".set_translation(Vector3(0,0,0))
		#get_tree().call_group("Gate", "close")
		emit_signal("released")