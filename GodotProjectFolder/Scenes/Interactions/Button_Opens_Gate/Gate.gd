extends Spatial

#"""

var moving = false
var is_closed = true
var opening = false

export var MOVEMENT_SPEED = 9
export var idle = false
var current_x_value = 0
export var start_x_value = 0
export var end_x_value = 9
var starting_scale = Vector3()

func _ready():
	starting_scale = scale

func _physics_process(delta):
	var direction = 0
	if(moving and !idle):
		if opening:
			direction = 1
		else:
			direction = -1
		var translation_amount = MOVEMENT_SPEED*delta*direction
		current_x_value += translation_amount
		translate_object_local(Vector3.RIGHT*translation_amount)
		var new_x_scale = lerp(0.2*starting_scale.x,starting_scale.x,1-current_x_value/end_x_value)
		scale.x = new_x_scale
		if current_x_value >= end_x_value or current_x_value <= start_x_value:
			moving = false

func open():
	opening = true
	moving = true
	$Sliding.play()


func close():
	opening = false
	moving = true
	$Sliding.play()

func is_closed():
	return is_closed

func is_idle():
	return idle

func set_starting_closed(start_closed:bool):
	is_closed = start_closed
#"""