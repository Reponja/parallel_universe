extends Node

var current_scene_code = ""
var old_level_time = 0.0

func _ready():
	pass

func theme_change(scene_code):
	if scene_code == current_scene_code:
		return
	if scene_code != "menu":
		$MainThemePlayer.stop()
	
	if scene_code == "menu":
		$MainThemePlayer.play()
		$DarkRoomPlayer.stop()
		$LaboratoryRatsPlayer.stop()
	
	if scene_code == "dimension_present":
		$DarkRoomPlayer.stop()
		var old_time = $DarkRoomPlayer.get_playback_position()
		$LaboratoryRatsPlayer.play(old_time)
	
	if scene_code == "dimension_past":
		$LaboratoryRatsPlayer.stop()
		var old_time = $LaboratoryRatsPlayer.get_playback_position()
		if current_scene_code != "dimension_present":
			old_time = 0.0
		$DarkRoomPlayer.play(old_time)
	
	current_scene_code = scene_code

func _on_LaboratoryRatsPlayer_finished():
	$LaboratoryRatsPlayer.play()


func _on_DarkRoomPlayer_finished():
	$DarkRoomPlayer.play()


func _on_MainThemePlayer_finished():
	$MainThemePlayer.play()
