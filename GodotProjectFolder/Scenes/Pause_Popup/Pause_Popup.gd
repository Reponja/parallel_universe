extends Node

signal button_pressed(button)
var master_volume = 0
var master_volume_slider = null

func _ready():
	master_volume = $"/root/Global_Singleton".get_setting("Game","MasterVolume")
	master_volume_slider = get_node("CanvasLayer/Main/RowsContainer/Scrollers/HSlider")
	master_volume_slider.value = master_volume


func _on_Continue_pressed():
	emit_signal("button_pressed","continue")

func _on_ToMenu_pressed():
	emit_signal("button_pressed","tomenu")

func _on_Restart_pressed():
	emit_signal("button_pressed","restart")

func _on_HSlider_value_changed(value):
	$"/root/Global_Singleton".set_setting("Game","MasterVolume",value)

func _on_Apply_pressed():
	emit_signal("button_pressed","apply")


func _on_Fullscreen_pressed():
	emit_signal("button_pressed","fullscreen")
